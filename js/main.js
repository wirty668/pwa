window.onload = () => {
  'use strict';

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./service_worker.js')
    .then(() => console.log("Service Worker Registered"))
    .catch(e => console.log(e));
  }
}
